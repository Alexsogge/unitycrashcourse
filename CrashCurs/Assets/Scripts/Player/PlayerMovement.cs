﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerMovement : MonoBehaviour
{

    public float walking_speed = 0.1f;
    public float jumpf = 100;
    public float rotation_speed = 0.01f;
    public float shootForce = 10;
    public GameObject projectilePrefab;

    private Rigidbody _rigidbody;
    

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Booom");
            GameObject projectile = Instantiate(projectilePrefab, transform.position + new Vector3(0, 1.6f, 0), transform.rotation * Quaternion.Euler(70, 90, 0));
            projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.rotation * Vector3.up * shootForce);
            Destroy(projectile, 4);
        }
    }

    void FixedUpdate()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");


        transform.Rotate(new Vector3(0, 1, 0) * rotation_speed * Time.deltaTime * horizontal);

        Vector3 force = new Vector3(1, 0, 0) * walking_speed * Time.deltaTime * vertical;

        _rigidbody.AddForce(transform.rotation * force);


        if (Input.GetKey(KeyCode.Space))
        {
            _rigidbody.AddForce(new Vector3(0, jumpf, 0));
        }


    }
}
